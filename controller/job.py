from flask import session
from database import db

class JobControl:
    def __init__(self):
        # please delete this function
        print("created")

    def editJobDetail(self, req):
        # We have to check if they are JID
        Title=req['Title']
        Deadline=req['Deadline']
        CreatedDate=req['CreatedDate']
        Description=req['Description']
        Budget=req['Budget']
        if(db.editJobDB(req['JID'],Title,Deadline,CreatedDate,Description,Budget)):
            return {"msg": "successful", "code": 201}
        else:
            return {"msg": "database error", "code": 201}