import psycopg2

connection_parameters = {
    'host':'localhost',
    'database': 'gddata',
    'user': 'postgres',
    'password': '0000',
    'port': '5433'
}
def testConnection():
    conn = None
    reserved = []
    status="unknown"
    try:
        conn = psycopg2.connect(**connection_parameters)
        cur = conn.cursor()
        cur.execute("""SELECT username FROM "Admin";""")
        reserved = cur.fetchall()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        status=error
    finally:
        if conn is not None:
            conn.close()
            status="connected"
        return reserved

def insert(sql):
    conn = None
    reserved = []
    status="unknown"
    try:
        conn = psycopg2.connect(**connection_parameters)
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute(sql)
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        status=error
    finally:
        if conn is not None:
            conn.close()
            status="connected"
        return status
def select(sql):
    # do something
    conn = None
    reserved = []
    status="unknown"
    try:
        conn = psycopg2.connect(**connection_parameters)
        cur = conn.cursor()
        cur.execute(sql)
        reserved = cur.fetchall()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        status=error
    finally:
        if conn is not None:
            conn.close()
            status="connected"
        return reserved
def update(sql):
    conn = None
    reserved = []
    status="unknown"
    try:
        conn = psycopg2.connect(**connection_parameters)
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute(sql)
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        status=error
    finally:
        if conn is not None:
            conn.close()
            status="connected"
        return status

def delete(sql):
    conn = None
    reserved = []
    status="unknown"
    try:
        conn = psycopg2.connect(**connection_parameters)
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute(sql)
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        status=error
    finally:
        if conn is not None:
            conn.close()
            status="connected"
        return status

def editJobDB(JID,Title,Deadline,CreatesDate,Description,Budget):
    conn = None
    status=True
    sql =  sql = """UPDATE public."Job" 
                    SET "Title"=%s, "Deadline"=%s, "CreatedDate"=%s, "Description"=%s, "Budget"=%s 
                    WHERE "JID" = %s;"""
    try:
        conn = psycopg2.connect(**connection_parameters)
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute(sql,(Title,Deadline,CreatesDate,Description,Budget,JID))
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        status=False
    finally:
        if conn is not None:
            conn.close()
        return status
