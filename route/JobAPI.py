from flask import Blueprint, request, jsonify
from flask_json import json_response
from database import db
from controller import JobControl

job_api = Blueprint('job_api', __name__)

@job_api.route('/API/job/editJob', methods=['POST'])
def editJob():
    req = request.get_json()
    jobCT = JobControl()
    res = jobCT.editJobDetail(req)
    code = res["code"]
    res = jsonify(res)
    del jobCT
    return res, code
    ''' test json for this api
        {
            "JID":"20180000",
            "Title": "testJobedit",
            "Deadline": "2018-08-15",
            "CreatedDate": "2018-08-1",
            "Description":"I'm test", 
            "Budget":"1000"
        }
        '''